#!/usr/bin/env python

from collections import OrderedDict
from datetime import datetime, timezone
import boto3
import botocore
import fileinput
import json
import logging
import os
import pandas as pd
import re
import requests

NR_API_KEY = os.getenv("NR_API_KEY")
NR_ACCOUNT_ID = os.getenv("NR_ACCOUNT_ID")

# RegEx to extract the information from gc.log - We need to keep them the order as they exit on the first match
GC_EVENTS = OrderedDict([
  ("FULL_GC", re.compile(r"(?P<datetime>\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}.\d+): (?P<time>[^:]+): \[Full GC \((?P<cause>[^\)]+)\) (?P<somedata>[^,]+), (?P<duration>[0-9\.]+) secs\]")),
  ("GC_PAUSE", re.compile(r"(?P<datetime>\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}.\d+): (?P<time>[^:]+): \[GC pause \((?P<action>[^\)]+)\) \((?P<collector>[^\)]+)\)(\s\((?P<cause>[^\)]+)\))?, (?P<duration>[\d\.]+) secs\]")),
  ("OTHER_GC_EVENTS", re.compile(r"(?P<datetime>\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}.\d+): (?P<time>[^:]+): \[GC (?P<event>[^\s]+)(?P<somedata>.*)?, (?P<duration>[0-9\.]+) secs\]")),
])
GC_MEM = re.compile(r"(?P<pre>.+)->(?P<post>.+)\((?P<total>.+)\)")

ELB_NAME = "alice-elb"
ELB_MIN_HEALTHY_HOSTS = 1


def mem_to_bytes(memory):
  '''
  Convert a string of the format <number>[b|k|M|G|T] to its equivalent number of bytes

  Parameters:
    memory(str): string representation of an amount of memory

  Returns:
    bytes(int): the number of bytes that string represents
  '''

  units = {"B": 1, "K": 2**10, "M": 2**20, "G": 2**30, "T": 2**40}

  memory = memory.upper()

  if not re.match(r" ", memory):
      memory = re.sub(r"([BKMGT])", r" \1", memory)

  number, unit = [string.strip() for string in memory.split()]

  return int(float(number) * units[unit])




class GC_Monitor:
  '''
  A class for GC monitoring and Tomcat management

  Usage:
    tail -F /path/to/gc.log 2>/dev/null | gcmonitor.py
  '''

  def __init__(self):
    logging.basicConfig(format="%(asctime)s - [%(name)s] - %(levelname)s - %(message)s")
    self.log = logging.getLogger("GC Monitor")
    self.log.setLevel(logging.getLevelName(
        os.environ.get('LOG_LEVEL', 'INFO')))

    self.__init_gc_history()

    self.log.info("GC Monitor started")


  def __init_gc_history(self):
    self.gc_history = pd.DataFrame(columns = ['timestamp', 'gc_type', 'duration', 'additional_data', 'additional_data_description'])


  def extract_data(self, line, regex):
    '''
    Extract data from a logline given a regular expression

    Parameters:
      line (str): Log line
      regex (compiled regex): Regular expression

    Returns:
      data (dict): fields extracted from the logline (if regex matched)
      None: if there's no match
    '''
    m = regex.match(line)

    if m:
      data = m.groupdict()

      data["timestamp"] = int(datetime.timestamp(datetime.fromisoformat(data["datetime"][:-2]+":"+data["datetime"][-2:])))
      data["duration"] = float(data["duration"])

      return data

    return None


  def record_gc_data(self, gc_event, data):
    '''
    Keep tracks of the last 5 minutes of GC history

    Parameters:
      gc_event(str): dype of GC event
      data(dict): data extracted from the log line
    '''
    self.log.debug("Recording data: {}".format(data))

    record_data = {
      "timestamp": data["timestamp"],
      "gc_type": gc_event,
      "duration": data["duration"],
    }

    if gc_event == "FULL_GC":
      mem_data = GC_MEM.match(data["somedata"]).groupdict()

      self.log.debug(mem_data)

      mem_started = mem_to_bytes(mem_data["pre"])
      mem_ended = mem_to_bytes(mem_data["post"])
      mem_total = mem_to_bytes(mem_data["total"])

      percentage_released = 0
      if mem_total != 0:
        percentage_released = (mem_started - mem_ended) * 100 / mem_total

      record_data["additional_data"] = percentage_released
      record_data["additional_data_description"] = "Percentage of memory released"

    self.gc_history = self.gc_history.append(record_data, ignore_index=True)

    # Clean and sort the history
    self.gc_history['datetime'] = pd.to_datetime(self.gc_history['timestamp'], unit='s')
    self.gc_history.set_index('datetime', drop=True, inplace=True)
    self.gc_history.sort_index(ascending=True, inplace=True)
    self.log.debug("Sorted Dataframe:\n{}".format(self.gc_history))

    truncation_mark = pd.Timestamp(datetime.utcnow()) - pd.Timedelta('5m')
    self.log.debug("Truncation mark: {}".format(truncation_mark))

    self.gc_history = self.gc_history.truncate(before=truncation_mark)

    # Save the truncated history
    self.gc_history = self.gc_history.reset_index(drop=True)
    self.gc_history.to_csv('/tmp/data.csv')
    self.log.debug("Dataframe:\n{}".format(self.gc_history))


  def nr_send_custom_event(self, custom_data):
    '''
    Send a payload to New Relic as a CustomEvent

    TODO: Improve error handling

    Parameters:
      custom_data (dict):A dictionary of key->value pairs to send to New Relic
    '''
    custom_data["eventType"] = "GC Monitor"
    custom_data["generator"] = "gc_monitor"

    requests.post("https://insights-collector.newrelic.com/v1/accounts/{}/events".format(NR_ACCOUNT_ID),
      data=json.dumps(custom_data),
      headers={"X-Insert-Key": NR_API_KEY}
    )


  def is_cluster_healthy(self, min_hosts):
    '''
    Check that at least `min_hosts` number of instances are registered to the cluster

    Parameters:
      min_hosts(int): minimum number of hosts to consider the cluster as healthy

    Returns:
      bool: when at least min_hosts hosts are in a healthy state
    '''
    client = boto3.client("elb")

    response = None
    healty_hosts = 0

    try:
      response = client.describe_load_balancers(LoadBalancerNames=[ELB_NAME])
    except botocore.exceptions.ClientError as error:
      self.log.debug("Error querying ELB: {}".format(error))

    if response and "LoadBalancerDescriptions" in response:
      healty_hosts = len(response["LoadBalancerDescriptions"][0]["Instances"])

    self.log.info("{} instances are healthy (minumum required: {})".format(healty_hosts, min_hosts))

    return healty_hosts >= min_hosts


  def should_restart(self):
    '''
    Indicates that the service should be restarted.

    Parameters:
      None

    Return:
      bool: whether the service should be restarted
    '''
    # More than 10 Full GC freeing less than 20% memory?
    full_gc_freed_20 = sum(self.gc_history[self.gc_history["gc_type"] == "FULL_GC"]["additional_data"] < 20.0)

    if full_gc_freed_20 >= 10:
        self.log.info("Number of Full GC that freed less than 20%: {}".format(full_gc_freed_20))

        return True

    # Total FullGC > 15%?
    total_gc_time = sum(self.gc_history["duration"])
    full_gc_time = sum(self.gc_history[self.gc_history["gc_type"] == "FULL_GC"]["duration"])

    full_gc_perc_time = 0
    if total_gc_time != 0:
      full_gc_perc_time = (full_gc_time * 100) / total_gc_time

    if full_gc_perc_time > 15:
        self.log.info("Time spent doing Full GC: {}%".format(full_gc_perc_time))

        return True

    self.log.info("Tomcat shouldn't be restarted")
    return False


  def restart_service(self, service="tomcat"):
    '''
    Restart a given service configured as a systemd service

    Parameters:
      service(str): service to restart (defaults to 'tomcat')
    '''
    # We don't want to keep stale data to take our decisions
    self.__init_gc_history()

    self.log.info("Restarting {}".format(service))
    os.system("systemctl restart {}".format(service))


  def run(self):
    '''

    '''
    for line in fileinput.input():
      self.log.debug(line)

      for gc_event, regex in GC_EVENTS.items():
        data = self.extract_data(line, regex)

        if data:
          self.record_gc_data(gc_event, data)

          nr_payload = {
            "gc_event": gc_event,
            "timestamp": data["datetime"],
            "duration": data["duration"],
          }

          if gc_event == "FULL_GC":
            nr_payload["cause"] = data["cause"]
          elif gc_event == "GC_PAUSE":
            nr_payload["action"] = data["action"]
            nr_payload["collector"] = data["collector"]
            nr_payload["cause"] = data["cause"]
          else:
            nr_payload["event"] = data["event"]

          # Send event to NR
          self.nr_send_custom_event(nr_payload)
          break

      if self.should_restart() and self.is_cluster_healthy(ELB_MIN_HEALTHY_HOSTS):
        self.restart_service()




if __name__ == "__main__":
  monitor = GC_Monitor()

  monitor.run()