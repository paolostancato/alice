# Alice Interview: Paolo Stancato #

## GC Challenge ##

### Setup ###

Copy the file `src/gcmonitor.py` somewhere in the $PATH

Install python dependencies:
```
pip install -r src/requirements.txt
```

Export the following variables (with the proper values)

AWS:
```
 export AWS_ACCESS_KEY_ID=<access-key>
 export AWS_SECRET_ACCESS_KEY=<secret-access-key>
 export AWS_DEFAULT_REGION=<default-region>
```

New Relic API Key:
```
 export NR_API_KEY=<api-key>
 export NR_ACCOUNT_ID=<account-id>
```

To change the log level:
```
export LOG_LEVEL=DEBUG
```

### Usage ###

Run the command as follows:

```
tail -F /path/to/gc.log 2>/dev/null | gcmonitor.py
```

This programs leverage the UNIX philosophy taking advantage of tail's ability to deal with file truncations